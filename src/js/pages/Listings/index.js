import React, { PureComponent } from 'react';
import Loadable from 'react-loadable';

import { Loading } from '@components/common';

const List = Loadable({
  loader: () => import('../../containers/List'),
  loading: Loading
});

class Listings extends PureComponent {
  render() {
    return (
      <List />
    );
  }
}

export default Listings;
