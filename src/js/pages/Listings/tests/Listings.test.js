import React from 'react';
// import ProductCard from '../index';
import { shallow } from 'enzyme';
// import ShallowRenderer from 'react-test-renderer/shallow';

import Listings from '../index';
// const renderer = new ShallowRenderer();

describe('<Listings />', () => {
  it('should render and match the snapshot', () => {
    const comp = shallow(<Listings />);
    expect(comp).toMatchSnapshot();
  });
});
