import {
  FETCH_LISTINGS_REQUEST,
  FETCH_LISTINGS_SUCCESS,
  FETCH_LISTINGS_ERROR
} from './actions';

const defaultState = {
  listings: [],
  isFetching: false,
  isFetched: false,
  error: null
};

const listingsReducer = (state = defaultState, action) => {
  console.log('action', action);
  switch (action.type) {
    case FETCH_LISTINGS_REQUEST:
      return {
        ...state, isFetching: true, isFetched: false
      };

    case FETCH_LISTINGS_SUCCESS:

      return {
        ...state, listings: action.listings, isFetching: false, isFetched: true
      };

    case FETCH_LISTINGS_ERROR:
      return {
        ...state, isFetching: false, isFetched: false, error: action.error
      };

    default:
      return state;
  }
};

export default listingsReducer;
