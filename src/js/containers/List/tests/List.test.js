import React from 'react';
// import ProductCard from '../index';
import { shallow } from 'enzyme';
// import ShallowRenderer from 'react-test-renderer/shallow';

import List from '../index';
import configureStore from 'redux-mock-store';

// create any initial state needed
const initialState = {};
// here it is possible to pass in any middleware if needed into //configureStore
const mockStore = configureStore();
let wrapper;
let store;
beforeEach(() => {
  //creates the store with any initial state or middleware needed
  store = mockStore(initialState);
  wrapper = shallow(<List store={store}/>);
});
// const renderer = new ShallowRenderer();

describe('<List />', () => {
  it('should render and match the snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
