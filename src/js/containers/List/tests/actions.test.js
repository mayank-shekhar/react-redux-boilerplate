// import React from 'react';
// import ProductCard from '../index';
// import { shallow } from 'enzyme';

import {
  fetchListingsRequest,
  fetchListingsSuccess,
  fetchListingsFailure,
  FETCH_LISTINGS_REQUEST,
  FETCH_LISTINGS_SUCCESS,
  FETCH_LISTINGS_ERROR
} from '../actions';

describe('Listings Page actions', () => {
  describe('fetchListingsRequest', () => {
    it('should return the correct constant', () => {
      expect(fetchListingsRequest()).toEqual({
        type: FETCH_LISTINGS_REQUEST,
      });
    });
  });
  describe('fetchListingsSuccess', () => {
    it('should return the correct constant', () => {
      expect(fetchListingsSuccess()).toEqual({
        type: FETCH_LISTINGS_SUCCESS,
      });
    });
  });
  describe('fetchListingsFailure', () => {
    it('should return the correct constant', () => {
      expect(fetchListingsFailure()).toEqual({
        type: FETCH_LISTINGS_ERROR,
      });
    });
  });
  // describe('fetchListings', () => {
  //     it('should get listings data from API', () => {
  //         fetchListings()
  //         expect(fetchListings()).toEqual({
  //             type: FETCH_LISTINGS_ERROR,
  //         });
  //     });
  // });
});
