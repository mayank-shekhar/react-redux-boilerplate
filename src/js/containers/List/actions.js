import {
  proxyApi
} from '@lib/api';
import generateActionCreator from '@lib/generateActionCreator';

export const FETCH_LISTINGS_REQUEST = 'FETCH_LISTINGS_REQUEST';
export const FETCH_LISTINGS_SUCCESS = 'FETCH_LISTINGS_SUCCESS';
export const FETCH_LISTINGS_ERROR = 'FETCH_LISTINGS_ERROR';

export const fetchListingsRequest = generateActionCreator(FETCH_LISTINGS_REQUEST);
export const fetchListingsSuccess = generateActionCreator(
  FETCH_LISTINGS_SUCCESS,
  'listings'
);
export const fetchListingsFailure = generateActionCreator(
  FETCH_LISTINGS_ERROR,
  'error'
);

export const fetchListings = () => {
  return dispatch => {
    dispatch(fetchListingsRequest());

    return proxyApi
      .get('/listings')
      .then(data => {
        console.log('Listings fetched', data);
        dispatch(fetchListingsSuccess(data));

        return Promise.resolve(data);
      })
      .catch(error => {
        console.log('Listings error', error);
        dispatch(fetchListingsFailure(error));

        return Promise.reject(error);
      });
  };
};
