import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import Loadable from 'react-loadable';
import { connect } from 'react-redux';

import { Loading } from '@components/common';
import { fetchListings } from './actions';
import css from './List.scss';

const ProductCard = Loadable({
  loader: () => import('../../components/common/ProductCard'),
  loading: Loading
});

class List extends PureComponent {

  static fetchData = ({ store }) => {
    console.log('Fetching Listings');
    return store.dispatch(fetchListings());
  };

  componentDidMount() {
    const { dispatch, listings: { isFetched } } = this.props;

    if (!isFetched) {
      dispatch(fetchListings());
    }
  }

  render() {
    const { listings } = this.props;
    return (
      <div className={css.listings}>
        <Helmet>
          <title>Listings Page</title>
        </Helmet>

        {
          typeof(listings.listings) !== undefined && listings.listings.length ? (
            <div>
              <h1>Product Listings</h1>
              <ul className={css.productList}>
                {
                  listings.listings.map((product, i) => {
                    return (
                      <li key={i}>
                        <ProductCard data={product} />
                      </li>
                    );
                  })
                }
              </ul>
            </div>
          ) : (
            <h1>No Product Listings</h1>
          )
        }
      </div>
    );
  }
}

List.propTypes = {
  listings: PropTypes.object,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  listings: state.listingsReducer
});

export default connect(mapStateToProps)(List);
