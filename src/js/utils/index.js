export const getAssetPath = (assetName) => {
  return process.env.LOCAL_ASSET_PATH + assetName;
};
