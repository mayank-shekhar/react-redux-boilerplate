import Error from '@pages/Error';
import Home from '@pages/Home';
import Listings from '@pages/Listings';

export default [
  { path: '/', exact: true, component: Home },
  { path: '/listings', exact: true, component: Listings },
  { path: '*', component: Error }
];
