import React from 'react';
import ProductCard from '../index';
import { shallow } from 'enzyme';

describe('ProductCard', () => {
  it('renders correctly', () => {
    const component = shallow(
      <ProductCard data={
        { id: 1,
          title: 'Paracetamol',
          description: 'Lorem ipsum doret',
          text: 'Learn React',
          image: 'https://via.placeholder.com/500x500.png'
        }} />);
    expect(component).toMatchSnapshot();
  });
});
