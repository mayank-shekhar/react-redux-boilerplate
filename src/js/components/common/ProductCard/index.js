import React from 'react';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';

import css from './ProductCard.scss';

const ProductCard = ({data}) => {
  return (
    <div className={css.productCard}>
      <div className={css.productImage}>
        <LazyLoad once offset={40}>
          <img src={data.image} alt={data.title} />
        </LazyLoad>
      </div>
      <div className={css.productMeta}>
        <h4>{data.title}</h4>
        <i>{data.text}</i>
        <p>{data.description}</p>
      </div>
    </div>
  );
};

ProductCard.propTypes = {
  data: PropTypes.object
};

export default ProductCard;
