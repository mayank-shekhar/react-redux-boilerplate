import each from 'lodash/each';

export const createActionTypes = (prefix, actionTypeList) => {
  const actionTypesObject = {};

  each(actionTypeList, (item) => {
    actionTypesObject[item] = `${prefix}/${item}`;
  });

  return actionTypesObject;
};
