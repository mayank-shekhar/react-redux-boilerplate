import axios from 'axios';

const headers = { 'Content-Type': 'application/json' };
const baseURL = process.env.INTERNAL_NODE_API_URL || '';
const proxyApi = axios.create({ baseURL, headers, timeout: 200000 });
const backendApi = axios.create({ baseURL: process.env.EXTERNAL_API_URL, headers, timeout: 200000 });

proxyApi.interceptors.response.use(
  (response) => Promise.resolve(response.data),
  (err) => Promise.reject(err.response.data)
);

backendApi.interceptors.response.use(
  (response) => Promise.resolve(response.data),
  (err) => Promise.reject(err.response.data)
);

export {
  proxyApi,
  backendApi
};
