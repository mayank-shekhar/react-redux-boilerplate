import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

// Import your reducers here
import listingsReducer from '@containers/List/reducer';

const rootReducer = (history) => {
  return combineReducers({
    router: connectRouter(history),
    listingsReducer
  });
};

export default rootReducer;
