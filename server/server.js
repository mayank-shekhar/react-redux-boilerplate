import path from 'path';
import express from 'express';
import helmet from 'helmet';
import compression from 'compression';
import Api from './api';
import cookieParser from 'cookie-parser';
import ReactRenderer from './renderer';
// import { httpsRedirect } from '$middleware';
import cors from 'cors';

const expressStaticGzip = require('express-static-gzip');

const env = process.env.NODE_ENV || 'development';
const app = new express();

// Secure with helmet
app.use(helmet());

// enable cors
app.use(cors());

// Ensures SSL in used in production.
// app.use(httpsRedirect({ enabled: env === 'production' }));

// parse cookies!
app.use(cookieParser());

// gzip
app.use(compression());

// Add middleware to serve up all static files
if (env === 'production') {
  app.use(
    '/assets',
    expressStaticGzip(
      path.join(__dirname, '../' + process.env.PUBLIC_OUTPUT_PATH), {
        enableBrotli: true,
        orderPreference: ['br', 'gzip', 'deflate']
      }),
    expressStaticGzip(path.join(__dirname, '../src/static/images'), {
      enableBrotli: true,
      orderPreference: ['br', 'gzip', 'deflate']
    }),
    expressStaticGzip(path.join(__dirname, '../src/static/fonts'), {
      enableBrotli: true,
      orderPreference: ['br', 'gzip', 'deflate']
    }),
    expressStaticGzip(path.join(__dirname, '../src/js/lib'), {
      enableBrotli: true,
      orderPreference: ['br', 'gzip', 'deflate']
    })
  );
} else {
  app.use(
    '/assets',
    express.static(path.join(__dirname, '../' + process.env.PUBLIC_OUTPUT_PATH)),
    express.static(path.join(__dirname, '../src/static/images')),
    express.static(path.join(__dirname, '../src/static/fonts')),
    express.static(path.join(__dirname, '../src/js/lib'))
  );
}


app.use(
  '/service-worker.js',
  express.static(path.join(__dirname, '../service-worker.js'))
);

// handle browsers requesting favicon
app.use(
  '/favicon.ico',
  express.static(path.join(__dirname, '../src/static/images/favicon/favicon.ico')) // eslint-disable-line
);

// Mount the REST API
app.use('/api', Api);

// Mount the react render handler

app.use('*', ReactRenderer);

module.exports = app;
