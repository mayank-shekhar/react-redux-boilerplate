import express from 'express';
import bodyParser from 'body-parser';
import listings from './listings';

const Api = express();

// always send JSON headers
Api.use((req, res, next) => {
  res.contentType('application/json');
  next();
});

// parse JSON body
Api.use(bodyParser.json());

// Add all API endpoints here
Api.use('/listings', listings);

export default Api;
