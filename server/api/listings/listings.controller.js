import find from 'lodash/find';

// Exported controller methods
export default {
  index,
  show
};

// This is an example to mock an async fetch from a database or service.
const getListings = async () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(require('./listings.fixture.js'));
    });
  });
};

export function show(req, res) {
  const id = parseInt(req.params.id);
  const data = require('./listings.fixture.js');
  const product = find(data, { id });

  return res.json(product);
}

export async function index(req, res) {
  const products = await getListings();
  return res.json(products);
}
