
import request from 'supertest';
import express from 'express';
import listings from '../index';

let app, agent;

beforeAll(async () => {
  app = express();
  app.use('/api/listings', listings);
  agent = request.agent(app.listen());
});

describe('GET /api/listings', function() {
  test('endpoint returns a list of listings', function() {
    return agent
      .get('/api/listings')
      .expect(200)
      .then(({ body }) => {
        expect(body.map(t => t.id)).toEqual([1, 2, 3, 4, 5, 6]);
      });
  });
});

describe('GET /api/listings/:id', function() {
  test('endpoint returns a specific listing', function() {
    return agent
      .get('/api/listings/1')
      .expect(200)
      .then(({ body }) => {
        expect(body.id).toEqual(1);
      });
  });
});
