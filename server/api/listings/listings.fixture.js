module.exports = [{
  id: 1,
  title: 'Paracetamol',
  description: 'Lorem ipsum doret',
  text: 'Learn React',
  image: 'https://via.placeholder.com/500x500.png'
},
{
  id: 2,
  title: 'Dolo',
  description: 'Lorem ipsum doret',
  text: 'Learn Redux',
  image: 'https://via.placeholder.com/500x500.png'
},
{
  id: 3,
  title: 'Paracetamol',
  description: 'Lorem ipsum doret',
  text: 'Start an app',
  image: 'https://via.placeholder.com/500x500.png'
},
{
  id: 4,
  title: 'Paracetamol',
  description: 'Lorem ipsum doret',
  text: 'Make it universally rendered',
  image: 'https://via.placeholder.com/500x500.png'
},
{
  id: 5,
  title: 'Paracetamol',
  description: 'Lorem ipsum doret',
  text: 'Enable code splitting',
  image: 'https://via.placeholder.com/500x500.png'
},
{
  id: 6,
  title: 'Paracetamol',
  description: 'Lorem ipsum doret',
  text: 'Build a kick ass app',
  image: 'https://via.placeholder.com/500x500.png'
}
];
