import './registerAliases';
import fs from 'fs';
import path from 'path';
import config from '@config';
import Loadable from 'react-loadable';
import chokidar from 'chokidar';
import merge from 'lodash/merge';
/* eslint-disable */
const env = process.env.NODE_ENV || 'development';
const dotenv = require('dotenv');

// HTML files are read as pure strings
require.extensions['.html'] = (module, filename) => {
  module.exports = fs.readFileSync(filename, 'utf8');
};

const loadEnvironmentVariables = () => {
  const currentEnv = process.env.BUILD_ENV;
  console.info('====================================');
  console.info('Current Environment is: ', currentEnv);
  console.info('====================================');
  console.info('====================================');
  console.info('Create a build using config: ', env);
  console.info('====================================');
  let defaultConfig = {};
  try {
    defaultConfig = dotenv.parse(fs.readFileSync('.env.default'));
    console.info('Default Environment Config \n', defaultConfig);
  } catch (e) {
    console.error('Default Config File not found');
  }
  let envConfig = dotenv.parse(fs.readFileSync('.env' + (env !== 'development' ? '.' + currentEnv : '')));
  console.info('Current Environment: ' + currentEnv + ' Config \n', envConfig);
  console.info('Mergeing Environment Config with Default Config \n');
  envConfig = merge({}, defaultConfig, envConfig);
  console.info('Merged Environment Config \n', envConfig);
  for (let k in envConfig) {
    if (!process.env[k])
      process.env[k] = envConfig[k];
  }
};

loadEnvironmentVariables();

dotenv.config();

if (env === 'development') {
  require('dotenv').config();

  // In development, we compile css-modules on the fly on the server. This is
  // not necessary in production since we build renderer and server files with
  // webpack and babel.
  require('css-modules-require-hook')({
    extensions: ['.scss'],
    generateScopedName: config.cssModulesIdentifier,
    devMode: true
  });

  // Add better stack tracing for promises in dev mode
  process.on('unhandledRejection', r => console.log(r));
}

const configureIsomorphicTools = function (server) {
  // configure isomorphic tools
  // this must be equal to the Webpack configuration's 'context" parameter
  const basePath = path.resolve(__dirname, '..');
  const ISOTools = require('webpack-isomorphic-tools');

  // this global variable will be used later in express middleware
  global.ISOTools = new ISOTools(config.isomorphicConfig).server(
    basePath,
    () => server
  );
};

const startServer = () => {
  const server = require('./server');
  const port = process.env.PORT || process.env.APPLICATION_PORT || 3000;
  const host = env === 'development' ? 'localhost' : (process.env.DEV_SERVER_HOSTNAME);

  if (!global.ISOTools) {
    configureIsomorphicTools(server);
  }

  return Loadable.preloadAll().then(() => {
    const isProdEnv = env === 'production' && process.env.BUILD_ENV !== 'development';

    // code to run on heroku for now. Delete this when merging.
    // delete this block
    return server.listen(port, error => {
      if (error) {
        console.error(error);
      } else {
        console.info(`Application server mounted on http://${host}:${port}.`);
      }
    });
    // delete this block

    if (isProdEnv) {
      const https = require('https');
      const key = fs.readFileSync(path.resolve('./ssl/private.key'), 'utf8');
      const cert = fs.readFileSync(path.resolve('./ssl/primary.crt'), 'utf8');
      const ca = fs.readFileSync(path.resolve('./ssl/intermediate.crt'), 'utf8');
      const sslOptions = {
        key,
        cert,
        ca
      };
      https.createServer(sslOptions, server).listen(port, error => {
        if (error) {
          console.error(error);
        } else {
          console.info(`Application server mounted on https://${host}:${port}.`);
        }
      });
    } else {
      return server.listen(port, error => {
        if (error) {
          console.error(error);
        } else {
          console.info(`Application server mounted on http://${host}:${port}.`);
        }
      });
    }
  });
};

// This check is required to ensure we're not in a test environment.
if (!module.parent) {
  let server;

  if (!config.enableDynamicImports) {
    startServer();
  } else {
    // Ensure react-loadable stats file exists before starting the server
    const statsPath = path.join(__dirname, '..', 'react-loadable.json');
    const watcher = chokidar.watch(statsPath, {
      persistent: true
    });

    console.info(`Checking/waiting for ${statsPath}...`);

    watcher.on('all', (event, path) => {
      if (event === 'add') {
        if (env === 'production') {
          watcher.close();
        }

        console.info(`Stats file found at ${path}, starting server...`);

        startServer().then(s => server = s);

      } else if (event === 'change') {
        if (env === 'production') {
          watcher.close();
        }

        console.info('Stats file changed, restarting server...');

        if (server) {
          // if the server is already started, restart it.
          server.close(() => {
            startServer().then(s => server = s);
          });
        } else {
          // otherwise, just start the server.
          startServer().then(s => server = s);
        }
      }
    });
  }
}
/* eslint-enable */
