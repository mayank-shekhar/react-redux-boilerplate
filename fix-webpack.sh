#!/usr/bin/env bash

OPEN_FILES_LIMIT=524288

echo "Fixing Open files limit for your machine"

echo kern.maxfiles=$OPEN_FILES_LIMIT | sudo tee -a /etc/sysctl.conf
echo kern.maxfilesperproc=$OPEN_FILES_LIMIT | sudo tee -a /etc/sysctl.conf
sudo sysctl -w kern.maxfiles=$OPEN_FILES_LIMIT
sudo sysctl -w kern.maxfilesperproc=$OPEN_FILES_LIMIT
sudo ulimit -n $OPEN_FILES_LIMIT

echo "Changed all values to $OPEN_FILES_LIMIT. Try running your server now!"
