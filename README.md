# React Redux PWA with SSR

Out of the box, this setup comes with:

- Server-side rendering with Express
- Code splitting with [dynamic imports](https://webpack.js.org/guides/code-splitting/#dynamic-imports) and [react-loadable](https://github.com/thejameskyle/react-loadable)
- Sane [webpack configurations](webpack/)
- JS hot reloading with [react-hot-loader (@next)](https://github.com/gaearon/react-hot-loader) and [webpack-dev-server](https://github.com/webpack/webpack-dev-server)
- CSS, SASS and [css-modules](https://github.com/css-modules/css-modules) support with hot reloading and no [flash of unstyled content](https://en.wikipedia.org/wiki/Flash_of_unstyled_content) ([css-hot-loader](https://github.com/shepherdwind/css-hot-loader))
- Routing with [react-router-v4](https://github.com/ReactTraining/react-router)
- Full production builds that do not rely on `@babel/node`.
- Offline Support using Offline plugin / Workbox [Offline Plugin](https://github.com/NekR/offline-plugin)


## Development Mode

Copy environment variables and edit them if necessary:

```
cp .env.example .env
```

### Install NodeJS and NPM
- Install NVM
```
if [ ! -f ~/.bashrc ]; then touch ~/.bashrc; fi
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
source ~/.bashrc
```
**Note**: If you are using ZSH you should run `source ~/.zsh_rc` instead of `source ~/.bashrc`
- Install Node@10.15.3 and NPM@6.9.0
```
nvm install 10.15.3
npm install -g npm@6.9.0
```

Then:

```
npm install
npm start
```

Direct your browser to `http://localhost:3000`.

## Production Builds

Add environment variables the way you normally would on your production system.

```
npm run prod:build
npm run serve
```

Or simply:

```
npm run prod
```

## Path Aliases

In `package.json`, there is a property named `_moduleAliases`. This object
defines the require() aliases used by both webpack and node.

Aliased paths are prefixed with one of two symbols, which denote different
things:

`@` - component and template paths, e.g. `@components`

`$` - server paths that are built by babel, e.g. `server/api`

Aliases are nice to use for convenience, and lets us avoid using relative paths
in our components:

```
// This sucks
import SomeComponent from '../../../components/SomeComponent';

// This is way better
import SomeComponent from '@components/SomeComponent';
```

You can add additional aliases in `package.json` to your own liking.

## Environment Variables

In development mode, environment variables are loaded by `dotenv` off the `.env`
file in your root directory. In production, you'll have to manage these
yourself.

## CSS Modules

This project uses [CSS Modules](https://github.com/css-modules/css-modules).
Class names should be in `camelCase`. Simply import the .scss file into your
component, for example:

```
├── components
│   ├── Header.js
│   ├── Header.scss
```

```
// Header.scss
.headerContainer {
  height: 100px;
  width: 100%;
}
```

```
// Header.js
import css from './Header.scss';

const Header = (props) => {
  return (
    <div className={css.headerContainer}>
      {...}
    </div>
  );
}

```

## Redux Devtools

This project supports the awesome [Redux Devtools Extension](https://github.com/zalmoxisus/redux-devtools-extension).
Install the Chrome or Firefox extension and it should just work.

## Pre-fetching Data for Server Side Rendering (SSR)

When rendering components on the server, you'll find that you may need to fetch
some data before it can be rendered. The [component renderer](server/renderer/handler.js)
looks for a `fetchData` method on the container component and its child
components, then executes all of them and only renders after the promises have
all been resolved.

```
//  As an ES6 class

class TodosContainer extends React.Component {
  static fetchData = ({ store }) => {
    return store.dispatch(fetchTodos());
  };
}

// As a functional stateless component

const TodosContainer = (props) => {
  const { todos } = props;
  return (
    // ...component code
  );
}

TodosContainer.fetchData = ({ store }) => {
  return store.dispatch(fetchTodos());
}
```

## Async / Await

This project uses `async/await`, available by default in Node.js v8.x.x or
higher. If you experience errors, please upgrade your version of Node.js.

## Running ESLint

```
npm run lint
```

Check the `.eslintignore` file for directories excluded from linting.

## Changing the public asset path

By default, assets are built into `build/public`. This path is then served by
express under the path `assets`. This is the public asset path. In a production
scenario, you may want your assets to be hosted on a CDN. To do so, just change
the `PUBLIC_ASSET_PATH` environment variant.

Example using Heroku, if serving via CDN:

```
heroku config:set PUBLIC_ASSET_PATH=https://my.cdn.com
```

Example using Heroku, if serving locally:

```
heroku config:set PUBLIC_ASSET_PATH=/assets
```
