export default {
  babelrc: false,
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          browsers: 'last 2 versions'
        }
      }
    ],
    '@babel/preset-react',
  ],
  plugins: [
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-transform-react-jsx',
    'transform-es2015-modules-commonjs',
    'react-loadable/babel',
    'transform-class-properties',
    'transform-export-extensions',
    'syntax-dynamic-import'
  ],
  env: {
    development: {
      plugins: ['react-hot-loader/babel']
    },
    production: {
      plugins: ['transform-react-remove-prop-types']
    }
  }
};
