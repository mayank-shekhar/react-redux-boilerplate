const
  fs = require('fs'),
  AWS = require('aws-sdk'),
  path = require('path'),

  mimeTypeByExtension = require('./mime-types');

const uploadImages = ({
  assetsDir,
  s3BucketName,
  s3Folder,
  exclude
}) => {
  const
    s3 = new AWS.S3({
      signatureVersion: 'v4'
    }),
    cloudfront = new AWS.CloudFront({
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    }),
    extensionOf = filePath => filePath.split('.').pop(),
    s3PathForUpload = relativeFilePath => `${s3Folder}/${relativeFilePath}`,
    allFilesIn = function (dir, filelist, parent) {
      const files = fs.readdirSync(dir);
      filelist = filelist || [];
      files.forEach(function (file) {
        if (fs.statSync(dir + file).isDirectory()) {
          filelist = allFilesIn(dir + file + '/', filelist, (parent ? parent + '/' : '') + file);
        } else {
          filelist.push(parent ? (parent + '/' + file) : file);
        }
      });
      return filelist;
    },
    uploadFile = (filePath, targetPath) => {
      fs.readFile(filePath, (error, fileContent) => { //eslint-disable-line
        if (error) {
          throw error;
        }
        if (!exclude.test(filePath)) {
          const fileStream = fs.createReadStream(filePath);
          fileStream.on('error', function (err) {
            console.log('File Error', err);
          });
          s3.upload({
            Bucket: s3BucketName,
            Key: targetPath,
            Body: fileStream,
            ContentType: mimeTypeByExtension[extensionOf(filePath) || 'text/plain'],
            ACL: 'public-read'
          }, (err) => {
            if (err) {
              console.log(`Failed uploading :  '${filePath}'!`, err);
            }
          });
        }
      });
    },
    inValidateCache = () => {
      let randomString = Math.random().toString(36).substring(7);

      const params = {
        DistributionId: process.env.DISTRIBUTION_ID,
        /* required */
        InvalidationBatch: {
          /* required */
          CallerReference: randomString,
          /* required */
          Paths: {
            /* required */
            Quantity: 1,
            /* required */
            Items: [
              `/pwa-app/${process.env.BUILD_ENV}/*`
            ]
          }
        }
      };
      cloudfront.createInvalidation(params, function (err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else console.log('Cache Invalidation Successful', data); // successful response
      });
    },
    relativePaths = allFilesIn(assetsDir, []);

  relativePaths.forEach(relativePath => {
    const
      fullPathToFile = path.join(assetsDir, relativePath),
      s3FolderPath = s3PathForUpload(relativePath);

    if (!fs.lstatSync(fullPathToFile).isDirectory()) {
      uploadFile(fullPathToFile, s3FolderPath);
    }
  });

  console.log('==============================================');
  console.log('==========Starting cache invalidation=========');
  console.log('==============================================');

  if (process.env.BUILD_ENV === 'production')
    inValidateCache();
};

const UploadAssetsPlugin = function (options) {
  this.options = options;
};

UploadAssetsPlugin.prototype.apply = function (compiler) {
  compiler.plugin('done', () => {
    uploadImages(this.options);
  });
};

module.exports = UploadAssetsPlugin;
