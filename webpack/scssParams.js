import sass  from 'node-sass';
import nodeSassUtils  from 'node-sass-utils';

const isDev = process.env.NODE_ENV === 'development';
const sassUtils = nodeSassUtils(sass);

const imagePath = () => {
  const
    localPath = `http://localhost:${process.env.APPLICATION_PORT}${process.env.LOCAL_ASSET_PATH}`,
    cdnPath = `${process.env.CDN_ASSET_PATH}/${process.env.S3_FOLDER}/${process.env.BUILD_ENV}/assets/images/`;
  return isDev ? localPath : cdnPath;
};
export default {
  urlFor(imageRelativePath) {
    return sassUtils.castToSass(`${imagePath()}${sassUtils.sassString(imageRelativePath)}`);
  },
  urlForImage(imageRelativePath) {
    return `${imagePath()}${imageRelativePath}`;
  }
};
