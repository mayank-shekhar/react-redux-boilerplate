import webpack from 'webpack';
import baseConfig from './base';
import CompressionPlugin from 'compression-webpack-plugin';
import BrotliPlugin from 'brotli-webpack-plugin';
import merge from 'webpack-merge';
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// const TerserPlugin = require('terser-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');

import config from '../config';

export default merge(baseConfig, {
  mode: 'production',
  devtool: '',
  output: {
    filename: '[name].[hash].js',
    chunkFilename: config.enableDynamicImports ? '[name].[hash].js' : undefined
  },
  optimization: {
    minimize: true,
    minimizer: [
      new UglifyJsPlugin({
        uglifyOptions: {
          compress: true,
          mangle: true,
          output: {
            comments: false
          },
          parallel: true,
          // Enable file caching
          cache: true,
          sourceMap: false,
        }
      }),
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          parser: require('postcss-safe-parser'),
          map: false,
        },
      }),
    ],
    splitChunks: {
      chunks: 'all',
      name: true,
    }
  },
  plugins: [
    new webpack.BannerPlugin({
      banner:
        'hash:[hash], chunkhash:[chunkhash], name:[name], ' +
        'filebase:[filebase], query:[query], file:[file]'
    }),
    new CompressionPlugin({
      filename: '[file].gz[query]',
      algorithm: 'gzip',
      test: /\.css$|\.js$|\.html$/,
      threshold: 10240,
      minRatio: 0.8,
      cache: true
    }),
    new BrotliPlugin({
      asset: '[path].br[query]',
      test: /\.(js|css|html|svg|png)$/,
      threshold: 10240,
      minRatio: 0.8
    }),
    new WorkboxPlugin.InjectManifest({
      swSrc: './service-worker.js'
    })
  ]
});
