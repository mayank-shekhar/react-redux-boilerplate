import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import Loadable from 'react-loadable';
import FontFaceObserver from 'fontfaceobserver';

import './styles';
import configureStore from '@store';
import App from '@containers/App';
const robotoObserver = new FontFaceObserver('Roboto', {});

// When Open Sans is loaded, add a font-family using Open Sans to the body
robotoObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
});

// Hydrate the redux store from server state.
const initialState = window.__INITIAL_STATE__;
const history = createBrowserHistory();
const store = configureStore(initialState, history);

// Render the application
window.main = () => {
  Loadable.preloadReady().then(() => {
    ReactDOM.hydrate(
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </Provider>,
      document.getElementById('app')
    );
  });
};
